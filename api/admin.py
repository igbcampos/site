from django.contrib import admin
from .models import User, Report, Loan

class UserAdmin(admin.ModelAdmin):
    list_display = ['user', 'date']
    list_display_links = ['user']
    search_fields = ['user', 'date']
    list_per_page = 30

class ReportAdmin(admin.ModelAdmin):
    list_display = [Report.short_description, 'solved']
    list_display_links = [Report.short_description]
    list_filter = ['solved']
    search_fields = ['description']
    list_per_page = 30

class LoanAdmin(admin.ModelAdmin):
    list_display = ['user', 'date']
    list_display_links = ['user']
    search_fields = ['user', 'date', 'status']
    list_per_page = 30

admin.site.register(User, UserAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(Loan, LoanAdmin)