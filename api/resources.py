from tastypie.resources import ModelResource, Resource
from api.models import User, Report, Loan
from tastypie.authorization import Authorization
import api.renew as renew

class UserResource(ModelResource):
	class Meta:
		resource_name = 'user'
		queryset = User.objects.all()
		allowed_methods = ['post']
		authorization = Authorization()

class ReportResource(ModelResource):
	class Meta:
		resource_name = 'report'
		queryset = Report.objects.all()
		allowed_methods = ['post']
		authorization = Authorization()

class LoanResource(ModelResource):
	class Meta:
		resource_name = 'loan'
		queryset = Loan.objects.all()
		allowed_methods = ['post']
		authorization = Authorization()

class RenewResource(Resource):
	class Meta:
		resource_name = 'renew'
		detail_allowed_methods = ['get']

	def get_list(self, request, **kwargs):
		renew.run()

		data = {}

		return self.create_response(request, data)