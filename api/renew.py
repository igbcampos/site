'''import os
from django.conf import settings
settings.configure(
    INSTALLED_APPS=[
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    ],
    DATABASES={
        'default': {
            'NAME': os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'db.sqlite3'),
            'ENGINE': 'django.db.backends.sqlite3'
        }
    },
    MIDDLEWARE_CLASSES=()
)
import django
django.setup()'''

from robobrowser import RoboBrowser
from bs4 import BeautifulSoup
import re
from api.models import User, Loan
import schedule
import time

'''from threading import Thread

def start_new_thread(function):
    def decorator(*args, **kwargs):
        t = Thread(target = function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator

@start_new_thread'''
def job():
    #time.strftime("%d/%m/%Y %H:%M:%S")

    users = User.objects.all().values()

    for user in users:
        br = RoboBrowser()
        br.open('https://sigaa.ufpi.br/sigaa/verTelaLogin.do')

        form = br.get_form()
        form['user.login'] = user['user']
        form['user.senha'] = user['password']
        br.submit_form(form)

        form = br.get_form()
        form["jscook_action"] = "menu_form_menu_discente_j_id_jsp_1325243614_85_menu:A]#{meusEmprestimosBibliotecaMBean.iniciarVisualizarEmprestimosRenovaveis}"
        br.submit_form(form)

        info = str(br.find(id = "painel-erros"))

        if "Você não possui empréstimos ativos renováveis." in info:
            loan = Loan(user=user['user'], date=time.ctime(), status="Você não possui empréstimos ativos renováveis.")
            loan.save()
        else :
            form = br.get_form()
            javaxFaces = br.find(id = "javax.faces.ViewState")
            javaxFaces = str(javaxFaces).split(" ")[4]
            javaxFaces = javaxFaces[7:len(javaxFaces) - 3]

            form_fields = br.find_all(id = re.compile('formularioRenovamaMeusEmprestimos:checkBoxSelecionaEmprestimo'))

            books = ""

            for input in form_fields:
                id = str(input).split(" ")[1]
                id = id[4:len(id) - 1]
                books += "&" + id + "=on";

            url = "https://sigaa.ufpi.br/sigaa/biblioteca/circulacao/renovaMeusEmprestimos.jsf?formularioRenovamaMeusEmprestimos=formularioRenovamaMeusEmprestimos&formularioRenovamaMeusEmprestimos:inputHiddenMensagemPaginaVisualiza=formularioRenovamaMeusEmprestimos:inputHiddenMensagemPaginaVisualiza" + books + "&formularioRenovamaMeusEmprestimos:cmdButtonConfirmarRenovacaoMeusEmprestimos=Renovar Empréstimos Selecionados&javax.faces.ViewState=" + javaxFaces
            br.open(url)

            info = str(br.find(id = "painel-erros"))

            if ("Empréstimos renovados com sucesso!" in info) or ("só poderá ser realizada em" in info):
                print("Deu certo!")

            loan = Loan(user=user['user'], date=time.ctime(), status=info)
            loan.save()

def run():
    #schedule.every().day.at("04:00").do(job)
    #schedule.every().day.at("08:00").do(job)
    #schedule.every().day.at("12:00").do(job)
    #schedule.every().day.at("16:00").do(job)
    #schedule.every().day.at("20:00").do(job)

    schedule.every().minute.at(":02").do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)