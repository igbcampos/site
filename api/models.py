from django.db import models
from django.utils import timezone

class User(models.Model):
	user = models.CharField(unique=True, max_length=50)
	password = models.CharField(max_length=100)
	date = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.user

class Report(models.Model):
	name = models.CharField(max_length=50)
	contact = models.CharField(max_length=100)
	description = models.TextField()
	created_date = models.DateTimeField(default=timezone.now)
	solved = models.BooleanField(default=False)

	def solve(self):
		self.solved = True
		self.save()

	def short_description(self):
		return self.description[:50]

class Loan(models.Model):
	user = models.CharField(max_length=50)
	date = models.CharField(max_length=100)
	status = models.TextField()